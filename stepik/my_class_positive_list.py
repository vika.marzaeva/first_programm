class NonPositiveError(Exception):
    pass


class PositiveList(list):
    def append(self, x):
        only_positive_list = []
        for item in x:
            if int(item) > 0:
                only_positive_list.append(item)
            else:
                raise NonPositiveError(str(x) + ' is non positive number')
        print(only_positive_list)


number = PositiveList()
number.append(6)
