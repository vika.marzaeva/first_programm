import csv

with open('my_file.csv') as f:
    reader = csv.reader(f)
    for row in reader:
        print(row)

with open('my_file.tsv') as f:
    reader = csv.reader(f, delimiter='\t')
    for row in reader:
        print(row)

students = [
    ['Bob', 24, '98989924309437824'],
    ['Sam', 22, '39493849380495300'],
    ['Sally', 24, '8923482309482009'],
    ['Met', 21, '92340895839303958']
]

with open('students.csv', 'w') as file:
    writer = csv.writer(file)
    for s in students:
        writer.writerow(s)
