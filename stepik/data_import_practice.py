import datetime

y, m, d = map(int, input().split())
delta = int(input())
new_day = datetime.datetime(y, m, d) + datetime.timedelta(delta)
print(new_day.strftime("%Y %-m %-d"))
