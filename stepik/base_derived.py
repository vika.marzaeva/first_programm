# a экземляр класса Derived. В свою очередь, Derived является наследником Base.
# В Base есть функция, которая присваивается переменной значение 0.
# При запуске a.add_one() функция add_one ищется, в первую очередь, в Derived.
# Там же она увеличивает переменную на 10. Итого, a = 0 + 10 = 10.
#
# b экземляр класса Derived. В свою очередь, Derived является наследником Base.
# В Base есть функция, которая присваивается переменной значение 0.
# При запуске b.add_many(3) функция add_many ищется сначала в Derived (не находит), а затем в родителе – Base.
# Там эта функция запускает цикл (3 итерации), который вызывает функцию add_one (ищет сначала в Derived),
# которая увеличивает значение переменной на 10. Итого, после цикла b = 0 + 10 + 10 + 10.

class Base:
    def __init__(self):
        self.val = 0

    def add_one(self):
        self.val += 1

    def add_many(self, x):
        for i in range(x):
            self.add_one()


class Derived(Base):
    def add_one(self):
        self.val += 10


a = Derived()
a.add_one()

b = Derived()
b.add_many(3)

print(a.val)
print(b.val)
