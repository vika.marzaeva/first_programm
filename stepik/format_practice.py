import requests

template = "Response from {0.url} with code {0.status_code}"

res = requests.get('http://docs.python.org/3.5')
print(template.format(res))
