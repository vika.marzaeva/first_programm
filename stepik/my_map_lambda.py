n, k = map(int, input().split())
print(n + k)

x = input().split()
xs = (int(i) for i in x)


def even(x):
    return x % 2 == 0


evens = list(filter(even, xs))
for i in evens:
    print(i)  # выведет только четные

x = input().split()
xs = (int(i) for i in x)

evens = list(filter(lambda x: x % 2 == 0, xs))
print(evens)
