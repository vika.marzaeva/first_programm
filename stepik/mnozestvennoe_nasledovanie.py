# класс примесь
# примесь - мы будем примешивать к нашему основному классу


class EvenLengthMixin:
    def even_length(self):
        return len(self) % 2 == 0


# класс наследуется от листа, от EvenLengthMixin
class Mylist(list, EvenLengthMixin):
    pass


class Mydict(dict, EvenLengthMixin):
    pass


print(Mylist.mro())
x = Mylist([1, 2, 3])
print(x.even_length())
x.append(6)
print(x.even_length())
y = Mydict()
y["key"] = "value"
y["new_key"] = "new_value"
print(y.even_length())
