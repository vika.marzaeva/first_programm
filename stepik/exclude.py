class EvenLengthMixin:
    def even_length(self):
        return len(self) % 2 == 0


class Mylist(list, EvenLengthMixin):
    pass


try:
    x = Mylist([1, 4, 3, 2, 7])
    x.sort()
    print(x)
except TypeError:
    print("Type error :(")

print("i can catch")
