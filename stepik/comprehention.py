x = [2, 3, -4, 0, 1]
y = [i * i for i in x if i > 0]
print(y)

z = [(z1, z2) for z1 in range(3) for z2 in range(3) if z2 >= z1]
print(z)

c = []
for c1 in range(3):
    for c2 in range(3):
        if c2>=c1:
            c.append((c1, c2))
print(c)
