import json

student1 = {
    'name': 'Garry',
    'years': 22
}
student2 = {
    'name': 'Bob',
    'years': 23
}

data = [student1, student2]
with open('my_students.json', 'w') as file:
    json.dump(data, file, indent=4, sort_keys=True)
