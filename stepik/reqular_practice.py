# x = 'hello\nworld'
# print(x)
#
# y = r'hello\nworld'  #raw
# print(y)
import re

pattern = 'abc'
string = 'babc'
match_object = re.match(pattern, string)
print(match_object)

pattern = 'abc'
string = 'babc'
match_object = re.search(pattern, string)
print(match_object)

pattern = 'a[a-c]c'  # 'a[a-zA-Z]c'
string = 'acc'
match_object = re.match(pattern, string)
print(match_object)
string = 'abc, acc, aac, abb'
all_inclusion = re.findall(pattern, string)
print(all_inclusion)

fixed = re.sub(pattern, 'abc', string)
print(fixed)
