import os.path

print(os.listdir())  # все файлы в текущей директории
print(os.listdir('../profit'))  # какие файлы в папке profit
print(os.getcwd())  # покажет pwd, то есть текущую папку
print(os.path.exists('text.txt'))  # True, если файл существуетв в текущей директории
print(os.path.exists('my_file.txt'))  # False, если файл не существует в текущей директории
print(os.path.exists('calc'))  # False, если папка не существует в текущей директории
print(os.path.isdir('../stepik'))
print(os.path.isfile('exclude.py'))
print(os.path.abspath('comprehention.py'))   # Абсолютный путь до файла
os.chdir('../even_to_zero')
print(os.getcwd())

