# Даны катеты прямоугольного треугольника.
# Найдите площадь, периметр и гипотенузу треугольника.


def area(a, b):
    s = (a * b) / 2
    return s


def perimeter(a, b):
    p = (a ** 2 + b ** 2) ** 0.5 + a + b
    return p


def hypotenuse(a, b):
    c = (a ** 2 + b ** 2) ** 0.5
    return c


if __name__ == '__main__':
    a = float(input('Введите величину первого катета: '))
    while a <= 0:
        print('Введите положительное число')
        a = float(input('Введите величину первого катета: '))

    b = float(input('Введите величину второго катета: '))
    while b <= 0:
        print('Введите положительное число')
        b = float(input('Введите величину первого катета: '))
    s = area(a, b)
    p = perimeter(a, b)
    c = hypotenuse(a, b)
    print('Площадь треугольника равна = ', s)
    print('Периметр равен = ', p)
    print('Гипотенуза равна = ', c)
