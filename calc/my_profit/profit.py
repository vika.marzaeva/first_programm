# Пользователь вводит количество дней, указывает процент скидки и вводит сумму.
# Рассчитать прибыль, если за каждый день сумма увеличивается на 3 $  и затем применяется скидка,
# то есть итоговая сумма еще увеличивается на данное число процентов.

def calculate_profit(days, discount, summa):
    summa = summa
    for i in range(days):
        summa = summa + 3
        summa = summa + summa * discount / 100
    return round(summa, 2)


if __name__ == '__main__':
    days = int(input('Введите количество дней: '))
    while days <= 0:
        print('Введите положительное число')
        days = int(input('Введите количество дней: '))

    discount = float(input('Введите процент скидки: '))
    while discount < 0:
        print('Введите положительное число')
        discount = float(input('Введите процент скидки: '))

    summa = float(input('Введите сумму: '))
    while summa <= 0:
        print('Введите положительное число')
        summa = float(input('Введите сумму: '))

    profit = calculate_profit(days, discount, summa)
    print('Прибыль равна = ', profit)
