# Дано пятизначное число. Цифры на четных позициях занулить. Например, из 12345 получается число 10305.

def change_even_position_to_zero(number):
    new_number = '0'.join(number[::2])+('0' if len(number) % 2 == 0 else '')
    return new_number


if __name__ == '__main__':
    number = input('Введите пятизначное число: ')
    print(len(number))
    while len(number) != 5:
        number = input('Введите пятизначное число: ')

    new_number = change_even_position_to_zero(number)
    print('Новое число = ', new_number)
