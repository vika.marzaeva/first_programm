from unittest import TestCase, main

from calc.calculator import calculator


class CalculatorTest(TestCase):
    def test_plus(self):
        self.assertEqual(calculator('2+2'), 4)

    def test_minus(self):
        self.assertEqual(calculator('4-2'), 2)

    def test_divide(self):
        self.assertEqual(calculator('6/2'), 3)

    def test_multiple(self):
        self.assertEqual(calculator('5*2'), 10)

    def test_no_sign(self):
        with self.assertRaises(ValueError) as e:
            calculator('lkdslfks')
        self.assertEqual('Выражение должно содержать хотя бы один знак (+-/*)',
                         e.exception.args[0])  # исключение и его нулевой аргумент - текст
    #
    # def test_two_sign(self):
    #     with self.assertRaises(ValueError) as e:
    #         calculator('2+2+2')
    #     self.assertEqual('Выражение должно содержать 2 числа и один знак', e.exception.args[0])
    #
    # def test_string(self):
    #     with self.assertRaises(ValueError) as e:
    #         calculator('a+b')
    #     self.assertEqual('Выражение должно содержать 2 числа и один знак', e.exception.args[0])


if __name__ == '__main__':
    main()
