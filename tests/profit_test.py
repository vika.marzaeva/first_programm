import pytest

from profit.profit import calculate_profit


@pytest.mark.parametrize("days, discount, summa, expected_result",
                         [(5, 10, 1000, 1630.66),
                          (10, 10, 2000, 5240.08),
                          (30, 10, 1000, 17992.23)])
def test_profit(days, discount, summa, expected_result):
    assert calculate_profit(days, discount, summa) == expected_result
