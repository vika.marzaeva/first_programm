import pytest

from triangle_problem.triangle_problem import area, perimeter, hypotenuse


@pytest.mark.parametrize("a,b,expected_result", [(3, 4, 6), (8, 6, 24), (5, 6, 15)])
def test_area(a, b, expected_result):
    assert area(a, b) == expected_result


def test_perimeter():
    assert perimeter(8, 6) == 24


def test_hypotenuse():
    assert hypotenuse(4, 3) == 5
