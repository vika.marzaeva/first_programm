# Дано три числа. Найти количество положительных чисел среди них.


def count_positive_numbers(numbers):
    positive_numbers = []
    for i in numbers:
        if int(i) >= 0:
            positive_numbers.append(i)
    amount = len(positive_numbers)
    return amount


if __name__ == '__main__':
    numbers = input('Введите 3 числа: ').split()
    while len(numbers) != 3:
        print('Введите 3 числа')
        numbers = input('Введите 3 числа: ').split()
    amount_of_positive_numbers = count_positive_numbers(numbers)
    print('Количество положительных чисел = ', amount_of_positive_numbers)
