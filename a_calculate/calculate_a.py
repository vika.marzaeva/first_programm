# Дано число a. Не пользуясь никакими арифметическими операциями кроме умножения,
# получите а) a^4 за две операции;
#         б) a^6 за три операции;
#         в) a^15 за пять операций.

def calculate_a4(a):
    a1 = a * a
    a2 = a1 * a1
    return a2


def calculate_a6(a):
    a1 = a * a
    a2 = a1 * a1
    a3 = a2 * a1
    return a3


def calculate_a15(a):
    a1 = a * a
    a2 = a1 * a
    a3 = a2 * a2
    a4 = a3 * a3
    a5 = a4 * a2
    return a5


if __name__ == '__main__':
    a = float(input('Введите a: '))
    a4 = calculate_a4(a)
    a6 = calculate_a6(a)
    a15 = calculate_a15(a)
    print('a^4 = ', a4)
    print('a^6 = ', a6)
    print('a^15 = ', a15)
