import time

time.clock = time.time


# def fib(n):
#     if n in {0, 1}:
#         return n
#     return fib(n - 1) + fib(n - 2)

def fib(x):
    if x == 0 or x == 1:
        return 1
    else:
        return fib(x - 1) + fib(x - 2)


while True:
    for x in range(5):
        print(x, fib(x), time.time())


def main():
    n = int(input())
    print(fib(n))


if __name__ == "__main__":
    main()
