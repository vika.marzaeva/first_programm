def list_sum(lst):
    result = 0
    for i in lst:
        result += i
    return result


def sum(a, b):
    return a + b


# def printab(a, b, *args):
#     print('positional argument a: ', a)
#     print('positional argument b: ', b)
#     print('additional arguments: ')
#     for arg in args:
#         print(arg)
#
#
# def printab2(a, b, **kwargs):
#     print('positional argument a: ', a)
#     print('positional argument b: ', b)
#     print('additional arguments: ')
#     for arg in kwargs:
#         print(arg, kwargs[arg])


def s(a, *vs, b=10):
    res = a + b
    for v in vs:
        res += v
    return res


lst = [14, 19]
y = sum(*lst)
z = list_sum([1, 2, 3])
# printab(10, 20, 30, 40, 50)
# printab2(10, 20, c=30, d=40, name='Vika')
a = s(11, 10, b=10)
print('a = ', a)
print(y)
print(z)
