class MoneyBox:
    def __init__(self, capacity):
        self.capacity = capacity
        self.count = 0

    def can_add(self, v):
        return self.count + v <= self.capacity

    def add(self, v):
        if self.can_add(v):
            self.count += v

x = MoneyBox(10)
x.add(5)
x.add(9)
x.add(3)
print(x.can_add(10))
