from math import factorial


def c(n, k):
    if k == 0:
        return 1
    elif k > n:
        return 0
    else:
        return c(n - 1, k) + c(n - 1, k - 1)


n, k = map(int, input().split())
print(c(n, k))


def c_fact(a, b):
    return int((factorial(a)) / (factorial(b) * factorial(a - b)))


n, k = map(int, input().split())
print(c(n, k))
a, b = map(int, input().split())
print(c_fact(a, b))
