from construct_class import Person1, Person2

p1 = Person1()
p1.set_name('Tom', 'Soyer')
print(p1.name, p1.surname)

p2 = Person2('Bob', 'Marley')
print(p2.name, p2.surname)
