def mm(f, itr):  # по сути это функция map
    for e in itr:
        yield f(e)


def add1(n: int) -> int:  # функция прибавляет к целому числу 1
    return n + 1


print(list(mm(add1, [5, 6, 7])))
print(list(map(add1, [7, 8, 9])))
