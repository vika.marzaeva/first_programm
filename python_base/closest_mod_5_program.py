def closest_mod_5(x):
    if x % 5 == 0:
        return x
    else:
        return (x + 5 - x % 5)


res = closest_mod_5(7)
print(res)
